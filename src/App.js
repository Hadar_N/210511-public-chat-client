import './App.css';

import styled from "styled-components";
import React, {useState, useEffect, useRef} from "react";

import io from "socket.io-client";
const socket = io("http://localhost:3030/", 
{
  "force new connection" : true,
  "transports" : ["websocket"]
});

function App() {
  const msgTextBox= useRef(undefined);
  const ChatDisplay= useRef(undefined);
  const [msgs,setMsgs] = useState([]);

  const sendChatMsg = (e) => {
    e.preventDefault();
    if(msgTextBox && msgTextBox.current.value) {
      socket.emit('chat message', msgTextBox.current.value);
      msgTextBox.current.value = '';
    }
  }

  useEffect(()=> {
    socket.on('chat message', (msg) => {
    setMsgs(currArr => [...currArr, msg]);
  })

  return () => socket.close();
},[]);

useEffect(()=> {
  if(ChatDisplay) {
    ChatDisplay.current.scrollTo(0,ChatDisplay.current.scrollHeight);
  }
},[msgs]);


  return (
    <div className="App">
      <header className="App-header">
        
        <Chatbox>
          <List ref={ChatDisplay}>
            {msgs.map((item,index) => 
              <li key={index}>{item}</li>
          )}
          </List>

          <ChatForm>
          <input placeholder="message" ref={msgTextBox}/>
          <button onClick={sendChatMsg}>Send</button>
          </ChatForm>
        </Chatbox>

      </header>
    </div>
  );
}

export default App;

const Chatbox = styled.div`
  position: relative;
  width: 70vw;
  height: 60vh;
  background-color: honeydew;
  color: black;
  // padding-bottom: 1.2rem;
`;

const ChatForm = styled.form`
  position: absolute;
  bottom:0;
  width: 100%;
  display: flex;
  height: 2.5rem;
  
  input {
    flex-grow: 1;
  }
  button {
    width: 5rem;
  }
`;

const List = styled.ul`
height: calc(100% - 3rem);
list-style-type: none;
text-align: left;
padding: .3rem;
margin: 0;
line-height: 1.3;
font-size: 18px;
overflow-y: scroll;
`;